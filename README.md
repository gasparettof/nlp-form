# NLP Form Web App with Django and Hugging Face Transformers

This project is a web application developed using Django that integrates with Hugging Face's Transformers library for natural language processing (NLP) tasks.

## Features
- Allow users to input text data and questions.
- Utilizes various models from Hugging Face's Transformers for question answering.
- Retains user input even after form submission for a seamless user experience.

## Setup
1. Clone the repository.
2. Install the required dependencies by running: pip install -r requirements.txt
3. Run the Django development server: python manage.py runserver

## Technologies Used
- Django
- Hugging Face Transformers
- HTML, CSS (for front-end)

Feel free to explore the project and contribute if you'd like to enhance its functionality further.

## License
This project is licensed under the [MIT License](LICENSE).



