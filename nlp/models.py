from django.db import models


# Create your models here.

class NLPForm(models.Model):
    context = models.TextField()
    question = models.CharField(max_length=255)
    hfmod = models.CharField(max_length=255)

    class Meta:
        app_label = 'nlp'
