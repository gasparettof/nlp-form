from django import forms
import logging
from .models import NLPForm
from transformers import AutoModelForQuestionAnswering, AutoTokenizer
from huggingface_hub import HfApi, ModelFilter


class NLPFormForm(forms.ModelForm):
    @staticmethod
    def get_available_models():
        available_models = []
        api = HfApi()
        try:
            models = api.list_models(search="bert", task="question-answering")
            models_list = list(models)
            models_list = sorted(models_list, key=lambda x: x.id)
            available_models.append((0, 'Choose a model'))
            for model_name in models_list:
                try:
                    available_models.append((model_name.id, model_name.id))
                except Exception as e:
                    pass
        except Exception as e:
            logging.error(f"Error while retrieving models: {e}")
        return available_models

    model_choices = get_available_models()
    hfmod = forms.ChoiceField(choices=model_choices)

    class Meta:
        model = NLPForm
        fields = ['context', 'question', 'hfmod']
