from django.urls import path
from . import views

urlpatterns = [
    path('nlp-form/', views.nlp_form_view, name='nlp_form'),
]

