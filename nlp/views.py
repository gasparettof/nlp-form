from django.shortcuts import render
from transformers import pipeline
from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import TerminalFormatter
import json

# Create your views here.
from .forms import NLPFormForm


def nlp_form_view(request):
    response = ""
    context_val = ""
    question_val = ""
    hfmod_val = ""
    if request.method == 'POST':
        form = NLPFormForm(request.POST or None)
        context_val = request.POST.get('context', '')
        question_val = request.POST.get('question', '')
        hfmod_val = request.POST.get('hfmod', '')

        if form.is_valid():
            hfmod = form.cleaned_data['hfmod']
            question_answering = pipeline("question-answering", model=str(hfmod), tokenizer=str(hfmod))
            context = form.cleaned_data['context']
            question = form.cleaned_data['question']
            response = question_answering(question=question, context=context)
        else:
            context_val = form.data.get('context')
            question_val = form.data.get('question')
            hfmod_val = form.data.get('model')
    else:
        form = NLPFormForm()

    return render(request, 'nlp/nlp_form.html', {'form': form, 'response': response, 'context_val': context_val, 'question_val': question_val, 'hfmod_val': hfmod_val})
